# -*- coding: utf-8 -*-
import datetime
from datetime import timedelta
from functools import update_wrapper

import pyphen
import spacy
from flask import Flask, jsonify, make_response, request, current_app
# import readability
import textstat

# import predict_relations
from english_model.classifiers_components.predictions import predict_random_forest as predict_random_forest_en
# from english_model.classifiers_components.predictions import predict_log_reg, predict_gaussian_nb
from english_model.classifiers_components.contextual_features import type_indicators as type_indicators_en
from german_model.classifiers_components.predictions import predict_random_forest as predict_random_forest_de
from german_model.classifiers_components.contextual_features import type_indicators as type_indicators_de


def crossdomain(origin=None, methods=None, headers=None, max_age=21600,
                attach_to_all=True, automatic_options=True):
    """Decorator function that allows crossdomain requests.
      Courtesy of
      https://blog.skyred.fi/articles/better-crossdomain-snippet-for-flask.html
    """
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))

    if headers is not None and not isinstance(headers, str):
        headers = ', '.join(x.upper() for x in headers)

    if not isinstance(origin, str):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        """ Determines which methods are allowed
        """
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        """The decorator function
        """

        def wrapped_function(*args, **kwargs):
            """Caries out the actual cross domain code
            """
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers
            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            h['Access-Control-Allow-Credentials'] = 'true'
            h['Access-Control-Allow-Headers'] = \
                "Origin, X-Requested-With, Content-Type, Accept, Authorization"
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)

    return decorator


app = Flask(__name__)
nlp_EN = spacy.load('en_core_web_sm')
nlp_DE = spacy.load('de_core_news_sm')
dic = pyphen.Pyphen(lang='de')

HELP_TEXT = """
Sorry, but I am new here. I cannot help you at the moment.
"""


def _server_time():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


@app.route('/', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def hello():
    return jsonify({'server_time': _server_time()})


@app.route('/help', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def help():
    return jsonify({'help': HELP_TEXT, 'server_time': _server_time()})


@app.errorhandler(404)
def page_not_found(_):
    return make_response(jsonify({'message': 'No interface defined for URL'}), 404)


@app.route('/analyze', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
def analyze():
    # print(request)
    # print(request.json)

    text = request.get_json().get('text').strip()
    language = request.get_json().get('language').strip()

    # print('Received input:')
    # print('"' + text + '"')
    # print('"' + language + '"')

    try:
        response = do_analyze(text, language)
        # print('Produced annotations:')
        # print(response)
        return jsonify(response)
    except BaseException as error:
        return make_response(jsonify({'message': 'ERROR: {}'.format(error)}), 400)


def predict_components(text_input, lan):
    # sentences = nltk.tokenize.sent_tokenize(text_input)
    # sentences = [sent.string.strip() for sent in doc.sents]

    if lan == "English":

        predictions = predict_random_forest_en.predict(text_input)
        doc = nlp_EN(text_input)
        sentences = [sent.text for sent in doc.sents]

        count = 0
        elements = []
        argumentative_length = 0.0
        non_argumentative_length = 0.0

        for p in predictions:
            # print(p)
            if p == "Claim":
                argumentative_length = argumentative_length + len([t for t in nlp_EN(sentences[count])])
                elements.append({
                    'id': count,
                    'label': 'claim',
                    'start': text_input.find(sentences[count]),
                    'length': len(sentences[count]),
                    'text': sentences[count],
                    'confidence': 1.
                })

            elif p == "Premise":
                argumentative_length = argumentative_length + len([t for t in nlp_EN(sentences[count])])
                elements.append({
                    'id': count,
                    'label': 'premise',
                    'start': text_input.find(sentences[count]),
                    'length': len(sentences[count]),
                    'text': sentences[count],
                    'confidence': 1.
                })
            elif p == "MajorClaim":
                argumentative_length = argumentative_length + len([t for t in nlp_EN(sentences[count])])
                elements.append({
                    'id': count,
                    'label': 'majorClaim',
                    'start': text_input.find(sentences[count]),
                    'length': len(sentences[count]),
                    'text': sentences[count],
                    'confidence': 1.
                })

            else:
                non_argumentative_length = non_argumentative_length + len([t for t in nlp_EN(sentences[count])])

            count += 1

        return elements, argumentative_length, non_argumentative_length

    elif lan == "Deutsch":

        predictions = predict_random_forest_de.predict(text_input)
        doc = nlp_DE(text_input)
        sentences = [sent.text for sent in doc.sents]

        count = 0
        elements = []
        argumentative_length = 0.0
        non_argumentative_length = 0.0

        for p in predictions:
            # print(p)
            if p == "Claim":
                argumentative_length = argumentative_length + len([t for t in nlp_DE(sentences[count])])
                elements.append({
                    'id': count,
                    'label': 'claim',
                    'start': text_input.find(sentences[count]),
                    'length': len(sentences[count]),
                    'text': sentences[count],
                    'confidence': 1.
                })

            elif p == "Premise":
                argumentative_length = argumentative_length + len([t for t in nlp_DE(sentences[count])])
                elements.append({
                    'id': count,
                    'label': 'premise',
                    'start': text_input.find(sentences[count]),
                    'length': len(sentences[count]),
                    'text': sentences[count],
                    'confidence': 1.
                })
            elif p == "MajorClaim":
                argumentative_length = argumentative_length + len([t for t in nlp_DE(sentences[count])])
                elements.append({
                    'id': count,
                    'label': 'majorClaim',
                    'start': text_input.find(sentences[count]),
                    'length': len(sentences[count]),
                    'text': sentences[count],
                    'confidence': 1.
                })

            else:
                non_argumentative_length = non_argumentative_length + len([t for t in nlp_DE(sentences[count])])

            count += 1

        return elements, argumentative_length, non_argumentative_length


"""
    English readability index - handled with a library
"""


def get_readability(text):
    return textstat.flesch_reading_ease(text)
    # r = readability.Readability(text)
    # fk = r.flesch_kincaid()

    # return fk.score


"""
    German readability index - handled locally
"""


def get_number_of_sentences(text):
    sentences = [sent.text.strip() for sent in text.sents]

    return len(sentences)


def count_syllables(token):
    split_token = dic.inserted(token)
    syllables = split_token.split("-")
    return len(syllables)


def FRE_German(text):
    count_s = 0
    doc = nlp_DE(text)

    tokens = [token.text for token in doc]

    for token in tokens:
        count_s += count_syllables(token)

    number_of_sentences = get_number_of_sentences(doc)

    asw = count_s / len(tokens)
    asl = len(tokens) / number_of_sentences

    fre = 180 - asl - (58.5 * asw)

    return fre


"""
    English discourse markers
"""


def get_discourse_coherence_EN(text):
    causal_markers = ["since", "because", "as"]
    consecutive_markers = ["thus", "thereby", "therefore"]
    adversative_markers = ["instead of", "otherwise", "on the other hand", "on the one hand", "however", "but",
                           "rather", "in turn"]
    concessive_markers = ["however", "nevertheless" "although", "even though", "whereby", "though"]
    conditional_markers = ["if", "whether", "provided", "when"]

    argumentative_discourse_markers = causal_markers + consecutive_markers + adversative_markers + concessive_markers + conditional_markers

    doc = nlp_EN(text)
    tokens = [token.text.lower() for token in doc]

    counter = 0
    for t in tokens:
        if t in argumentative_discourse_markers:
            counter += 1

    sentences = [sent.text for sent in doc.sents]

    return counter, sentences


"""
    German discourse markers
"""


def get_discourse_coherence_DE(text):
    causal_markers = ["da", "denn", "weil"]
    consecutive_markers = ["also", "dadurch", "daher", "darum", "deshalb", "deswegen", "drum"]
    adversative_markers = ["anstatt", "anderenfalls", "andererseits", "einerseits", "hingegen", "jedoch", "sondern",
                           "statt", "stattdessen", "vielmehr", "wiederum", "zum einen", "zum anderen"]
    concessive_markers = ["allerdings", "dennoch", "gleichwohl", "nichtsdestotrotz", "nichtsdestoweniger", "obschon",
                          "obwohl", "trotzdem", "wenngleich", "wobei", "zwar"]
    conditional_markers = ["falls", "ob", "sofern", "wenn"]

    argumentative_discourse_markers = causal_markers + consecutive_markers + adversative_markers + concessive_markers + conditional_markers

    doc = nlp_DE(text)
    token_list = [token.text.lower() for token in doc]

    counter = 0
    for t in token_list:
        if t in argumentative_discourse_markers:
            # print(t)
            counter += 1

    sentences = [sent.text for sent in doc.sents]

    return counter, sentences


# TODO replace this function with the correct implementation
def do_analyze(text, lan):
    scores = {}

    # elements
    elements, argumentative_length, non_argumentative_length = predict_components(text, lan)
    elements = sorted(elements, key=lambda e: e['start'])

    # relations
    relations = relations_heuristic_context(elements, lan)
    # unsupported relations
    number_of_claims, number_of_unsupported_claims = unsupported_claims(elements, relations)

    # scores
    if lan == "English":
        score_discourse = get_discourse_coherence_EN(text)[0] / (len(get_discourse_coherence_EN(text)[1]) - 1)
        score_readability = get_readability(text) / 100
        score_unsupported = number_of_unsupported_claims / number_of_claims
        scores['argumentative'] = {
            'description': 'Argumentative',
            'score': argumentative_length / (argumentative_length + non_argumentative_length),
            'details': 'Not available'
        }
        scores['helpful'] = {
            'description': 'Readability',
            'score': score_readability,
            'details': readability_text(score_readability, lan)
        }
        # scores['structure'] = {'description': 'Structure', 'score': .33, 'details': structure_text(lan)}
        scores['discourse'] = {
            'description': 'Coherence',
            'score': score_discourse,
            'details': discourse_text(score_discourse, lan)
        }
        scores['unsupported'] = {
            'description': 'Persuasiveness',
            'score': 1 - score_unsupported,
            'details': unsupported_text(score_unsupported, lan)
        }

    elif lan == "Deutsch":
        score_discourse = get_discourse_coherence_DE(text)[0] / (len(get_discourse_coherence_DE(text)[1]) - 1)
        score_readability = FRE_German(text) / 100
        score_unsupported = number_of_unsupported_claims / number_of_claims

        scores['argumentative'] = {
            'description': 'Argumentativ',
            'score': argumentative_length / (argumentative_length + non_argumentative_length),
            'details': 'Not available'
        }
        scores['helpful'] = {
            'description': 'Lesbarkeit',
            'score': score_readability,
            'details': readability_text(score_readability, lan)
        }
        # scores['structure'] = {'description': 'Struktur', 'score': .33, 'details': structure_text(lan)}
        scores['discourse'] = {
            'description': 'Stimmigkeit des Textes',
            'score': score_discourse,
            'details': discourse_text(score_discourse, lan)
        }
        scores['unsupported'] = {
            'description': 'Überzeugungsfähigkeit',
            'score': 1 - score_unsupported,
            'details': unsupported_text(score_unsupported, lan)
        }

    return {
        'elements': elements,
        'relations': relations,
        'scores': scores,
        # 'learningProgress': 0.33
    }


def unsupported_claims(elements, relations):
    number_of_claims = 0
    number_of_unsupported_claims = 0
    for e in elements:
        if e['label'] == "claim":
            # print("claim found")
            number_of_claims += 1
            supported = False

            for r in relations:
                # print(e['id'], r['srcElem'], r['trgElem'])
                if r['srcElem'] == e['id'] or r['trgElem'] == e['id']:
                    supported = True
                    break
            if not supported:
                number_of_unsupported_claims += 1
    # print("number claims: ", number_of_claims)
    # print("number unsupported claims: ", number_of_unsupported_claims)

    return number_of_claims, number_of_unsupported_claims


"""
def relations_heuristic(elements, lan):
    relations = []
    _prev = None
    _prev_claim = None
    for i in range(len(elements)):
        if elements[i]['label'] == 'claim':
            _prev_claim = elements[i]
        if elements[i]['label'] == 'premise' and _prev_claim is not None:  # _prev is not None:
            if lan == 'English':
                relations.append({
                    'srcElem': elements[i]['id'],  # source element identifier
                    'trgElem': _prev_claim['id'],  # _prev['id'], # target element identifier
                    'label': 'supports',
                    'confidence': 1.
                })
            elif lan == 'Deutsch':
                relations.append({
                    'srcElem': elements[i]['id'],  # source element identifier
                    'trgElem': _prev_claim['id'],  # _prev['id'], # target element identifier
                    'label': 'unterstüzt',
                    'confidence': 1.
                })

    return relations
"""


def relations_heuristic_context(elements, lan):
    temp_relations = []
    major_claims = []
    claims = []
    premises = []

    """  Setting the ids of all elements to start from 1 """
    for i in range(len(elements)):
        elements[i]['id'] = i + 1

    """
        Creating a list for major claims, claims and premises. Each list contains:
        - major_claims list - a tuple with two elements:
            1. The major claim as is in the elements list
            2. A dict with two values:
                + 'previous': the claim that is preceding this major claim (None if not existing)
                + 'next': the claim that is following this major claim (None if not existing)
        
        - claims list - a tuple with two elements:
            1. The claim as is in the elements list
            2. A dict with two values:
                + 'prev_premise': the premise that is preceding this claim (None if not existing)
                + 'next_premise': the premise that is following this claim (None if not existing)
                + 'prev_major_claim': the major claim preceding this claim (None if not existing)
                + 'next_major_claim': the major claim following this claim (None if not existing)

        - premises list - a tuple with two elements:
            1. The premise as is in the elements list
            2. A dict with two values:
                + 'previous': the claim or major claim that is preceding this premise (None if not existing)
                + 'next': the claim or major claim that is following this premise (None if not existing)
    """

    for i in range(len(elements)):

        ### Major Claims
        if elements[i]['label'] == 'majorClaim':
            prev_claim = None
            next_claim = None

            for elem in elements[i::-1]:
                if elem['label'] == 'claim':
                    prev_claim = elem['id']
                    break

            for elem in elements[i:]:
                if elem['label'] == 'claim':
                    next_claim = elem['id']
                    break

            relative_premises = {'previous': prev_claim,
                                 'next': next_claim}
            major_claims.append((elements[i], relative_premises))

        ### Claims
        if elements[i]['label'] == 'claim':
            prev_premise = None
            next_premise = None
            prev_major_claim = None
            next_major_claim = None

            for elem in elements[i::-1]:
                if elem['label'] == 'premise':
                    prev_premise = elem['id']
                    break
                elif elem['label'] == 'majorClaim':
                    prev_major_claim = elem['id']
                    break

            for elem in elements[i:]:
                if elem['label'] == 'premise':
                    next_premise = elem['id']
                    break
                elif elem['label'] == 'majorClaim':
                    next_major_claim = elem['id']
                    break

            relative_premises = {'prev_premise': prev_premise,
                                 'next_premise': next_premise,
                                 'prev_major_claim': prev_major_claim,
                                 'next_major_claim': next_major_claim}
            claims.append((elements[i], relative_premises))

        ### Premises
        elif elements[i]['label'] == 'premise':
            prev_claim = None
            next_claim = None

            for elem in elements[i:0:-1]:
                if elem['label'] == 'claim':
                    prev_claim = elem['id']
                    break

            for elem in elements[i:]:
                if elem['label'] == 'claim':
                    next_claim = elem['id']
                    break

            relative_claims = {'previous': prev_claim,
                               'next': next_claim}
            premises.append((elements[i], relative_claims))

    """ 
        Will contain all the used premises from the first iteration (see below) and exclude them from the second
        iteration (see below).
        If this is not done, a complex web of relations may be created for some documents (maybe we can use it with
        more complex relations).
    """
    # used_premises_ids = []

    if lan == 'English':

        """
            Iterating over all major claims. If the type indicators bellow exist in each major claim, then a claim 
            will be assigned to the major claim in the relations list. Overview of the iteration:
            
            - If forward context exists in major claim:
                + Assign the following claim as a supporting element
                
            - If backward, rebuttal or thesis context exists in claim:
                + Assign the preceding claim as a supporting element
        """

        for majorClaim in major_claims:

            if forward_context_en(majorClaim[0]['text']):
                if majorClaim[1]['next'] is not None:
                    temp_relations.append({
                        'srcElem': majorClaim[1]['next'],
                        'trgElem': majorClaim[0]['id'],
                        'label': 'supports',
                        'confidence': 1.
                    })

                elif backward_context_en(majorClaim[0]['text']) or rebuttal_context_en(majorClaim[0]['text']) \
                        or thesis_context_en(majorClaim[0]['text']):
                    if majorClaim[1]['previous'] is not None:
                        temp_relations.append({
                            'srcElem': majorClaim[1]['previous'],
                            'trgElem': majorClaim[0]['id'],
                            'label': 'supports',
                            'confidence': 1.
                        })

        """ 
            Iterating over all claims. If the type indicators bellow exist in each claim, then a premise will
            be assigned to the claim in the relations list. Overview of the iteration:

            - If forward context exists in claim:
                + Assign the premise that follows as a supporting element
                + Assign this claim as support to the following major claim
            - If forward, backward or rebuttal context exists in claim:
                + Assign the preceding premise as a supporting element
                + Assign this claim as support to the preceding major claim
            - In any other case:
                + Assign this claim as support to the preceding major claim
        """

        for claim in claims:

            if forward_context_en(claim[0]['text']):
                if claim[1]['next_premise'] is not None:
                    temp_relations.append({
                        'srcElem': claim[1]['next_premise'],
                        'trgElem': claim[0]['id'],
                        'label': 'supports',
                        'confidence': 1.
                    })
                    # used_premises_ids.append(claim[1]['next_premise'])

                if claim[1]['next_major_claim'] is not None:
                    temp_relations.append({
                        'srcElem': claim[0]['id'],
                        'trgElem': claim[1]['next_major_claim'],
                        'label': 'supports',
                        'confidence': 1.
                    })

            elif backward_context_en(claim[0]['text']) or rebuttal_context_en(claim[0]['text']) \
                    or thesis_context_en(claim[0]['text']):
                if claim[1]['prev_premise'] is not None:
                    temp_relations.append({
                        'srcElem': claim[1]['prev_premise'],
                        'trgElem': claim[0]['id'],
                        'label': 'supports',
                        'confidence': 1.
                    })
                    # used_premises_ids.append(claim[1]['prev_premise'])
                if claim[1]['prev_major_claim'] is not None:
                    temp_relations.append({
                        'srcElem': claim[0]['id'],
                        'trgElem': claim[1]['prev_major_claim'],
                        'label': 'supports',
                        'confidence': 1.
                    })

            else:
                if claim[1]['prev_major_claim'] is not None:
                    temp_relations.append({
                        'srcElem': claim[0]['id'],
                        'trgElem': claim[1]['prev_major_claim'],
                        'label': 'supports',
                        'confidence': 1.
                    })

        """
            Iterating over the previously unused premises. For each premise that has not yet been used, the following
            rules apply:

            - If forward context exists in the premise:
                + The next claim is assigned a relation with this premise
            - If backward, rebuttal or a combination of thesis context and a relative position <0.8 out of 1:
                + The previous claim is assigned a relation with this premise
            - If thesis context exists and the relative position is >= 0.8:
                + The next claim is assigned a relation with this premise
            - In any other case:
                + The current premise is assigned to the previous claim

            (For points 2 and 3, the relative position is calculated by dividing the index pf the current element in
            the elements list by the length of the elements list. This is done to account for premises that are 
            close to the end of the document supporting the conclusion.)
        """

        for premise in premises:

            # if premise[0]['id'] not in set(used_premises_ids):
            if forward_context_en(premise[0]['text']):
                if premise[1]['next'] is not None:
                    temp_relations.append({
                        'srcElem': premise[0]['id'],
                        'trgElem': premise[1]['next'],
                        'label': 'supports',
                        'confidence': 1.
                    })

            elif backward_context_en(premise[0]['text']) or rebuttal_context_en(premise[0]['text']) or (
                    thesis_context_en(premise[0]['text']) and (
                    (elements.index(premise[0]) + 1) / len(elements) < 0.8)):
                if premise[1]['previous'] is not None:
                    temp_relations.append({
                        'srcElem': premise[0]['id'],
                        'trgElem': premise[1]['previous'],
                        'label': 'supports',
                        'confidence': 1.
                    })

            elif thesis_context_en(premise[0]['text']) and ((elements.index(premise[0]) + 1) / len(elements) < 0.8):
                if premise[1]['next'] is not None:
                    temp_relations.append({
                        'srcElem': premise[0]['id'],
                        'trgElem': premise[1]['next'],
                        'label': 'supports',
                        'confidence': 1.
                    })

            else:
                if premise[1]['previous'] is not None:
                    temp_relations.append({
                        'srcElem': premise[0]['id'],
                        'trgElem': premise[1]['previous'],
                        'label': 'supports',
                        'confidence': 1.
                    })

    elif lan == 'Deutsch':

        for majorClaim in major_claims:

            if forward_context_de(majorClaim[0]['text']):
                if majorClaim[1]['next'] is not None:
                    temp_relations.append({
                        'srcElem': majorClaim[1]['next'],
                        'trgElem': majorClaim[0]['id'],
                        'label': 'supports',
                        'confidence': 1.
                    })

                elif backward_context_de(majorClaim[0]['text']) or rebuttal_context_de(majorClaim[0]['text']) \
                        or thesis_context_de(majorClaim[0]['text']):
                    if majorClaim[1]['previous'] is not None:
                        temp_relations.append({
                            'srcElem': majorClaim[1]['previous'],
                            'trgElem': majorClaim[0]['id'],
                            'label': 'supports',
                            'confidence': 1.
                        })

        for claim in claims:

            if forward_context_de(claim[0]['text']):
                if claim[1]['next_premise'] is not None:
                    temp_relations.append({
                        'srcElem': claim[1]['next_premise'],
                        'trgElem': claim[0]['id'],
                        'label': 'supports',
                        'confidence': 1.
                    })
                    # used_premises_ids.append(claim[1]['next_premise'])

                if claim[1]['next_major_claim'] is not None:
                    temp_relations.append({
                        'srcElem': claim[0]['id'],
                        'trgElem': claim[1]['next_major_claim'],
                        'label': 'supports',
                        'confidence': 1.
                    })

            elif backward_context_de(claim[0]['text']) or rebuttal_context_de(claim[0]['text']) \
                    or thesis_context_de(claim[0]['text']):
                if claim[1]['prev_premise'] is not None:
                    temp_relations.append({
                        'srcElem': claim[1]['prev_premise'],
                        'trgElem': claim[0]['id'],
                        'label': 'supports',
                        'confidence': 1.
                    })
                    # used_premises_ids.append(claim[1]['prev_premise'])
                if claim[1]['prev_major_claim'] is not None:
                    temp_relations.append({
                        'srcElem': claim[0]['id'],
                        'trgElem': claim[1]['prev_major_claim'],
                        'label': 'supports',
                        'confidence': 1.
                    })

        for premise in premises:

            # if premise[0]['id'] not in set(used_premises_ids):
            if forward_context_de(premise[0]['text']):
                if premise[1]['next'] is not None:
                    temp_relations.append({
                        'srcElem': premise[0]['id'],
                        'trgElem': premise[1]['next'],
                        'label': 'unterstüzt',
                        'confidence': 1.
                    })

            elif backward_context_de(premise[0]['text']) or rebuttal_context_de(premise[0]['text']) or (
                    thesis_context_de(premise[0]['text']) and (
                    (elements.index(premise[0]) + 1) / len(elements) < 0.8)):
                if premise[1]['previous'] is not None:
                    temp_relations.append({
                        'srcElem': premise[0]['id'],
                        'trgElem': premise[1]['previous'],
                        'label': 'unterstüzt',
                        'confidence': 1.
                    })

            elif thesis_context_de(premise[0]['text']) and ((elements.index(premise[0]) + 1) / len(elements) < 0.8):
                if premise[1]['next'] is not None:
                    temp_relations.append({
                        'srcElem': premise[0]['id'],
                        'trgElem': premise[1]['next'],
                        'label': 'unterstüzt',
                        'confidence': 1.
                    })

            else:
                if premise[1]['previous'] is not None:
                    temp_relations.append({
                        'srcElem': premise[0]['id'],
                        'trgElem': premise[1]['previous'],
                        'label': 'unterstüzt',
                        'confidence': 1.
                    })

    """ Check to remove duplicate relations """
    relations = []
    for val in temp_relations:
        if val not in relations:
            relations.append(val)

    return relations


def forward_context_en(sentence):
    return type_indicators_en.forward_context(sentence)


def backward_context_en(sentence):
    return type_indicators_en.backward_context(sentence)


def rebuttal_context_en(sentence):
    return type_indicators_en.rebuttal_context(sentence)


def thesis_context_en(sentence):
    return type_indicators_en.thesis_context(sentence)


def forward_context_de(sentence):
    return type_indicators_de.forward_context(sentence)


def backward_context_de(sentence):
    return type_indicators_de.backward_context(sentence)


def rebuttal_context_de(sentence):
    return type_indicators_de.rebuttal_context(sentence)


def thesis_context_de(sentence):
    return type_indicators_de.thesis_context(sentence)


# Not used
def structure_text(lan):
    if lan == 'Deutsch':
        return 'Versuche beim nächsten Mal, deine Argumentationsstruktur zu verbessern.'
    elif lan == 'English':
        return 'Try to improve your argumentation structure next time.'


def readability_text(score, lan):
    if lan == "English":
        if score <= 0.50:
            text = 'Try to form shorter sentences and use simpler words to increase the readability of your text.'
        else:
            text = 'Your text is easy to read. Nevertheless, try to formulate your statements more clearly and to form short concise sentences to improve the readability and thus the comprehensibility of your text even further.'
    elif lan == "Deutsch":
        if score <= 50:
            text = 'Versuche kürzere Sätze zu formen und einfachere Wörter zu benutzen, um die Lesbarkeit deines Textes zu erhöhen.'
        else:
            text = 'Dein Text ist gut lesbar. Versuche dennoch Deine Aussagen noch etwas klarer zu formulieren und kurze prägnante Sätze zu formen, um die Lesbarkeit und damit die Verständlichkeit deines Textes noch weiter zu verbessern.'
    return text


def discourse_text(score, lan):
    if lan == "English":
        if score <= 0.50:
            text = 'Coherence is measured by the recognized links between your claims and your premises. Try to find examples, facts, or explanations to support each claim. Use connecting words (so-called connectors) to increase the coherence of your text.'
        else:
            text = 'Your text is well coherent. Your statements are generally supported by premises. To further increase coherence, try using clearer connecting words between statements.'
    elif lan == "Deutsch":
        if score <= 0.50:
            text = 'Die Stimmigkeit wird über die erkannten Verknüpfungen zwischen Deinen Claims und Deinen Prämissen gemessen. Versuche zu jeder Deine Aussage stützende Beispiele, Fakten oder Erklärungen zu finden. Nutze Verbindungswörter (sogenannte Konnektoren), um den Stimmigkeit Deines Textes zu erhöhen. <a href="http://deutschstundeonline.de/wp-content/uploads/2017/11/Konnektoren.pdf" target="_blank">Hier</a> findest du einen Übersicht über verschiedene Konnektoren.'
        else:
            text = 'Dein Text ist gut kohärent. Deine Aussagen werden in der Regel durch Prämissen gestützt. Um die Stimmigkeit weiter zu erhöhen, versuche klarere Verbindungswörter zwischen den einzelnen Aussagen zu nutzen.'

    return text


def unsupported_text(score, lan):
    if lan == "English":
        if score <= 0.50:
            text = 'The persuasiveness of your text is measured by the proportion of supported arguments in your text. Try to add more arguments and support them with premises to make your text more convincing.'
        else:
            text = 'Your text is very persuasive. Try to pick up potential counter-arguments and refute them to make your text even more convincing.'
    elif lan == "Deutsch":
        if score <= 0.50:
            text = 'Die Überzeugungsfähigkeit deines Textes wird über den Anteil der unterstützen Argumente in deinem Text gemessen. Versuche noch weitere Argumente hinzufügen und diese durch Prämissen zu unterstützen, um deinen Text überzeugender zu machen.'
        else:
            text = 'Dein Text hat eine gute Überzeugungsfähigkeit. Versuche noch auch mal potentielle Gegenargumente aufzugreifen und diese zu wiederlegen, um deinen Text noch überzeugender zu machen.'

    return text


if __name__ == "__main__":
    DEFAULT_HOST = '0.0.0.0'  # 127.0.0.1
    DEFAULT_PORT = '5130'

    host = DEFAULT_HOST
    port = DEFAULT_PORT
    print("Trying to start server at '{}':'{}'".format(host, port))

    app.run(host=host, port=port, debug=False, use_reloader=False)
