# ARTIST - ARgumentative wriTIng SupporT


<div align="center">
    <img src="utilities/templates/artist8.png" width="800px"</img> 
</div>


# Motivation
The study investigates a new generation of digital support for academic writing and offers deep insights into how AI technology can change academic writing and impacts changes in instructional approaches and adaptive coaching.

![Screenshot](utilities/templates/artist.png)


The framework provides **visual feedback on the quality of student-written argumentative essays**. By mining *argumentative components* and their *relationships* and assessing a set of different *quality dimensions* of the written text, it provides continuous and adaptive feedback to the user.

Type of Feedback:
* **In-text highlighting** of argumentative components (major claims, claims and premises)
* **Graph-based visualization** of argumentative relationships (support)
* **Chart-based visualization** of quality dimensions (readability, coherence and persuasiveness)

For more information, see [https://ai4writing.net/](https://ai4writing.net/).

# Usage
This video demonstrates the usage of artist: [artist.mkv](./images/artist.mkv)


# Online Demo
*TO DO...*


# Contributors (alphabetical order)
* Christina Niklaus
* Jannis Katis
* Siegfried Handschuh



<div align="center">
    <img src="utilities/templates/ICS-HSG_Logo_EN.jpg" width="500"</img> 
</div>



# Installation
We suggest to use a virtual environment.
Successfully tested with Python3.9

(1) Download:

    https://gitlab.com/ds-unisg/artist.git

(2) Run frontend server:

    docker-compose up --build
    
(3) Usage: Open the [index.html](./index.html) file in your browser.
    


