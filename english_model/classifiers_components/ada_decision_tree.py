from sklearn.ensemble import AdaBoostClassifier
import spacy
from sklearn.metrics import accuracy_score, classification_report
import dataframe
import pickle
import pandas as pd


nlp = spacy.load('en_core_web_sm')


cv, X_train, X_test, y_train, y_test = dataframe.create_train_test_split()


classifier = AdaBoostClassifier(n_estimators=50, learning_rate=1)
classifier.fit(X_train, y_train.values.ravel())

#pickle.dump(classifier, open("Gaussian_NB_model.pkl", 'wb'))

# Predict Class
y_pred = classifier.predict(X_test)
print(y_pred)

# Accuracy
accuracy = accuracy_score(y_test, y_pred)
print("adaboosted decision tree: ", accuracy)


### Classification Report
target_names = ['MajorClaim', 'Claim', 'Premise', 'None']
print(classification_report(y_test, y_pred, target_names=target_names))


pickle.dump(classifier, open("../models/decision_tree/ada_decision_tree.pkl", 'wb'))
pickle.dump(cv, open("../models/decision_tree/count_vectorizer_ada_decision_tree.pkl", 'wb'))

