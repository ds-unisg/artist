# import spacy
# from spacy.lang.de.examples import sentences
# from collections import OrderedDict
# import numpy as np

# nlp = spacy.load('en_core_news_sm')

# doc = nlp(sentences[0])
# print(doc.text)
# for token in doc:
   # print(token.text, token.pos_, token.dep_)


def get_dep_tags(sentence):
    pos_list = [token.dep_ for token in sentence]
    # print(pos_list)
    return pos_list

# get_dep_tags(doc)


### the function tree_depth() returns the depth of the dependency tree for one sentence
def root_depth(node, depth, depths):
    depths[node.orth_] = depth
    if node.n_lefts + node.n_rights > 0:
        return [root_depth(child, depth + 1, depths) for child in node.children], depths


def tree_depth(sen):
    depths = {}
    root_depth(sen.root, 0, depths)
    return max(depths.values())
