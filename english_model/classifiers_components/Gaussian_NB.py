import spacy
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.metrics import accuracy_score, classification_report
import dataframe
import pickle
import pandas as pd


nlp = spacy.load('en_core_web_sm')


cv, X_train, X_test, y_train, y_test = dataframe.create_train_test_split()


classifier = GaussianNB()
classifier.fit(X_train, y_train.values.ravel())


# Predict Class
y_pred = classifier.predict(X_test)
print(y_pred)

# Accuracy
accuracy = accuracy_score(y_test, y_pred)
print("Gaussian NB: ", accuracy)


### Classification Report
target_names = ['MajorClaim', 'Claim', 'Premise', 'None']
print(classification_report(y_test, y_pred, target_names=target_names))


pickle.dump(classifier, open("../models/Gaussian_NB/gaussian_NB.pkl", 'wb'))
pickle.dump(cv, open("../models/Gaussian_NB/count_vectorizer_gaussian_NB.pkl", 'wb'))





