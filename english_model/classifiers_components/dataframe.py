import pandas as pd
import spacy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MultiLabelBinarizer
from spacy.matcher import Matcher

import sys
  
# appending a path
sys.path.append("classifiers_components")

from structural_features import sentence_closes_with_question_mark
from structural_features import sentence_length
from indicators import reference_to_first_person
from indicators import argumentative_discourse_markers
from contextual_features import shared_phrases
from contextual_features import type_indicators
from syntactic_features import ner, parse_tree_depth, contains_adverb, \
    contains_verb, contains_modal_verb
from syntactic_features import tense_of_main_verb, number_of_subclauses
from syntactic_features import dependency

nlp = spacy.load('en_core_web_sm')

# TODO operate on clausal level!


def get_label(sentence, annotations):
    for ann in annotations:
        if ann[0] in sentence:
            assigned_label = ann[1]
            return assigned_label

    return "None"


"""The following 2 functions are relevant for the contextual features"""


### This function returns all the noun chunks of a sentence - used for the introduction and conclusion
def noun_chunks(sen):
    nouns = []

    for noun in sen.noun_chunks:
        nouns.append(noun)

    return nouns


### This function returns the verb phrases of a sentence - used for the introduction and conclusion
def verb_phrases(sen):
    ### found those patterns at https://stackoverflow.com/questions/47856247/extract-verb-phrases-using-spacy
    ### maybe we can use more/less
    pattern = [{'POS': 'VERB', 'OP': '?'},
               {'POS': 'ADV', 'OP': '*'},
               {'POS': 'AUX', 'OP': '*'},
               {'POS': 'VERB', 'OP': '+'}]

    ### instantiate a Matcher instance
    matcher = Matcher(nlp.vocab)
    matcher.add("Verb phrase", [pattern])

    ### call the matcher to find matches
    verbPhrases = [sen[start:end] for _, start, end in matcher(sen)]

    return verbPhrases


def create_dataframe(f):
    df = pd.DataFrame.from_dict(f)

    cv = CountVectorizer(max_features=1000, ngram_range=(1, 3))  # min_df=0.1, max_df=0.7

    mlb = MultiLabelBinarizer()

    X1 = cv.fit_transform(df.text)
    # print(X1.toarray())
    # print(cv.get_feature_names())
    df = df.drop(columns='text')
    count_vect_df = pd.DataFrame(X1.todense(), columns=cv.get_feature_names())
    # print(pd.concat([df, count_vect_df], axis=1))

    # X1 = mlb.fit_transform(df.ner)
    # df = df.drop(columns='ner')
    # df = df.join(pd.DataFrame(X1, columns=mlb.classes_))

    # X1 = mlb.fit_transform(df.pos)
    # df = df.drop(columns='pos')
    # df = df.join(pd.DataFrame(X1, columns=mlb.classes_))

    # X1 = mlb.fit_transform(df.dep)
    # df = df.drop(columns='dep')
    # df = df.join(pd.DataFrame(X1, columns=mlb.classes_))

    # X = mlb.fit_transform(df.label)
    # df = df.drop(columns='label')
    # df = df.join(pd.DataFrame(X, columns=mlb.classes_))

    # print(df.to_string())

    combined_df = pd.concat([df, count_vect_df], axis=1)

    # pickle.dump(cv, open('GaussianNB/cv.pkl', 'wb'))

    return combined_df, cv


def create_data_set(three_classes):

    f = []

    for i in range(1, 403):
        f_in = open("../data/Corpus/%s.txt" % i, "r", encoding="utf-8")
        text = f_in.read()

        f_ann = open("../data/Corpus/%s.ann" % i, "r")
        line = f_ann.readline()
        annotations = []

        while line:
            li = line.split("\t")

            if li[0].startswith("T"):
                annotation = li[2]
                label = li[1].split(" ")[0]

                if not three_classes:
                    if label in ('Claim', 'Premise'):
                        label = 'Argumentative'

                annotations.append((annotation.strip(), label))

            line = f_ann.readline()

        # for a in annotations:
            # print(a)

        doc = nlp(text)

        sentences = [sent for sent in doc.sents]

        ### calculating the average tokens of a sentence - relative for Structural Features
        total = 0

        for p in range(len(sentences)):
            total += len(sentences[p])

        avg_sent_length = total / len(sentences)

        ### to be used in some contextual features (from shared_phrases) - can also be moved away from the dataframe
        ### into the features but that would mean sending unnecessarily sending a lot of sentences
        ### relative for Contextual Features
        intro_nouns = noun_chunks(sentences[0])
        conc_nouns = noun_chunks(sentences[len(sentences) - 1])
        intro_verbs = verb_phrases(sentences[0])
        conc_verbs = verb_phrases(sentences[len(sentences) - 1])

        ### extracting the number of subclauses from the whole document - relative for Syntactic Features
        total_subclauses = number_of_subclauses.num_of_subclauses(doc)

        ### max dependency tree depth for the whole document - relative for Syntactic Features
        doc_parse_tree_depth = parse_tree_depth.parse_tree_depth(doc)

        for j in range(len(sentences)):
            features = {}

            if sentences[j] is not None:

                sen = str(sentences[j])

                features["label"] = get_label(sen, annotations)
                features["text"] = sen

                """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ STRUCTURAL FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

                ### newly added - greatly (!) increased accuracy
                if j == 0:
                    features["is_first"] = True
                else:
                    features["is_first"] = False

                if j == len(sentences) - 1:
                    features["is_last"] = True
                else:
                    features["is_last"] = False

                features["relative_position"] = j / len(sentences)
                features["number_preceding_components"] = j
                features["number_following_components"] = len(sentences) - j - 1
                features["token_ratio"] = len(sentences[j]) / avg_sent_length  # numeric
                features["sentence_length"] = sentence_length.get_sentence_length(sentences[j])  # numeric
                features["closing_question_mark"] = sentence_closes_with_question_mark.\
                    sentence_closes_with_question_marks(sentences[j])  # bool

                """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ INDICATOR FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

                features["causal_markers"] = argumentative_discourse_markers.contains_causal_markers(
                    sentences[j])  # bool
                features["conditional_markers"] = argumentative_discourse_markers.contains_conditional_markers(
                    sentences[j])  # bool
                features["adversative_markers"] = argumentative_discourse_markers.contains_adversative_markers(
                    sentences[j])  # bool
                features["consecutive_markers"] = argumentative_discourse_markers.contains_consecutive_markers(
                    sentences[j])  # bool
                features["concessive_markers"] = argumentative_discourse_markers.contains_concessive_markers(
                    sentences[j])  # bool
                features["argumentative_discourse_markers"] = \
                    argumentative_discourse_markers.contains_argumentative_markers(sentences[j])  # bool
                features["first_person"] = reference_to_first_person.contains_first_person(sentences[j])  # bool

                """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ CONTEXTUAL FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

                ### newly added - looking for argumentation in the preceding component. if the component is
                ### the first sentence of the document, the values are the same as the next 4 features
                if j - 1 >= 0:
                    features["preceding_thesis_indicator"] = type_indicators.thesis_context(str(sentences[j - 1]))
                    features["preceding_rebuttal_inidcator"] = type_indicators.rebuttal_context(str(sentences[j - 1]))
                    features["preceding_forward_context"] = type_indicators.forward_context(str(sentences[j - 1]))
                    features["preceding_backward_context"] = type_indicators.backward_context(str(sentences[j - 1]))
                elif j == 0:
                    features["preceding_thesis_indicator"] = False  # type_indicators.thesis_context(sen)
                    features["preceding_rebuttal_inidcator"] = False  # type_indicators.rebuttal_context(sen)
                    features["preceding_forward_context"] = False  # type_indicators.forward_context(sen)
                    features["preceding_backward_context"] = False  # type_indicators.backward_context(sen)

                ### newly added
                features["thesis_indicator"] = type_indicators.thesis_context(sen)  # bool
                features["rebuttal_indicator"] = type_indicators.rebuttal_context(sen)  # bool
                features["forward_context"] = type_indicators.forward_context(sen)  # bool
                features["backward_context"] = type_indicators.backward_context(sen)  # bool

                ### newly added - looking for argumentation in the following component. if the component is
                ### the last sentence of the document, the values are the same as the previous 4 features
                if j + 1 <= len(sentences) - 1:
                    features["following_thesis_indicator"] = type_indicators.thesis_context(str(sentences[j + 1]))
                    features["following_rebuttal_inidcator"] = type_indicators.rebuttal_context(str(sentences[j + 1]))
                    features["following_forward_context"] = type_indicators.forward_context(str(sentences[j + 1]))
                    features["following_backward_context"] = type_indicators.backward_context(str(sentences[j + 1]))
                elif j == len(sentences) - 1:
                    features["following_thesis_indicator"] = False  # type_indicators.thesis_context(sen)
                    features["following_rebuttal_inidcator"] = False  # type_indicators.rebuttal_context(sen)
                    features["following_forward_context"] = False  # type_indicators.forward_context(sen)
                    features["following_backward_context"] = False  # type_indicators.backward_context(sen)

                ### comparing noun chunks of component with noun chunks in first and last sentences of essay, return
                ### the number of matches
                features["intro_shared_noun_chunks"] = shared_phrases.shared_noun_phrases(sentences[j],
                                                                                          intro_nouns)  # numeric
                features["conc_shared_noun_chunks"] = shared_phrases.shared_noun_phrases(sentences[j],
                                                                                         conc_nouns)  # numeric

                ### comparing verb phrases of component with verb phrases in first and last sentences of essay, return
                ### the number of matches
                features["intro_shared_verb_phrases"] = shared_phrases.shared_verb_phrases(sentences[j],
                                                                                           intro_verbs)  # numeric
                features["conc_shared_verb_phrases"] = shared_phrases.shared_verb_phrases(sentences[j], conc_verbs)  # numeric

                ### the number of total nouns and verb phrases in the introduction and conclusion sentences
                features["intro_noun_chunks"] = len(intro_nouns)
                features["conc_noun_chunks"] = len(conc_nouns)
                features["intro_verb_phrases"] = len(intro_verbs)
                features["conc_verb_phrases"] = len(conc_verbs)

                """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ SYNTACTIC AND OTHER FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

                features["contains_adverb"] = contains_adverb.contains_adverb(sentences[j])  # bool
                features["contains_modal"] = contains_modal_verb.contains_modal_verb(sentences[j])  # bool
                features["contains_verb"] = contains_verb.contains_verb(sentences[j])  # bool
                features["contains_named_entities"] = ner.contains_ner(sentences[j])  # bool

                ### newly added
                features["parse_tree_depth"] = doc_parse_tree_depth  # numeric
                features["sentence_dependency_depth"] = dependency.tree_depth(sentences[j])
                features["tense"] = tense_of_main_verb.get_tense(sentences[j])  # numeric {"-1": past, "0": present, "1": future}
                features["number_of_subclauses"] = total_subclauses  # numeric

                # features["pos"] = pos.get_pos_tags(sen)
                # features["dep"] = dependency.get_dep_tags(sen)
                # features["ner"] = ner.get_ner(sen) # used above as a boolean feature

                f.append(features)
                # print(features)

    # print(f)

    dataset = create_dataframe(f)

    return dataset


def create_train_test_split():
    dataset, cv = create_data_set(True)
    ### display the dimensions of the dataset

    # analyse_dataset.analyse(dataset)

    # save DataFrame
    dataset.to_csv('analytical_base_table.csv', index=None)

    y = dataset.loc[:, ['label']]  # y = dataset.label
    X = dataset.drop(['label'], axis=1)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)  # random_state=1234

    return cv, X_train, X_test, y_train, y_test
