import spacy
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.metrics import accuracy_score, classification_report
import dataframe
import pickle
import pandas as pd


nlp = spacy.load('en_core_web_sm')


cv, X_train, X_test, y_train, y_test = dataframe.create_train_test_split()

classifier = MultinomialNB()
classifier.fit(X_train, y_train.values.ravel())

#pickle.dump(classifier, open("Gaussian_NB_model.pkl", 'wb'))

# Predict Class
y_pred = classifier.predict(X_test)
print(y_pred)

# Accuracy
accuracy = accuracy_score(y_test, y_pred)
print("multinomial NB: ", accuracy)


### Classification Report
target_names = ['MajorClaim', 'Claim', 'Premise', 'None']
print(classification_report(y_test, y_pred, target_names=target_names))


pickle.dump(classifier, open("../models/multinomial_NB/multinomial_NB.pkl", 'wb'))
pickle.dump(cv, open("../models/multinomial_NB/count_vectorizer_multinomial_NB.pkl", 'wb'))

