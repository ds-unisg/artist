# import pandas as pd

import spacy
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, classification_report
import dataframe
import pickle

nlp = spacy.load('en_core_web_sm')


cv, X_train, X_test, y_train, y_test = dataframe.create_train_test_split()


classifier = LogisticRegression(max_iter=10000)

# tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-2, 1e-3, 1e-4, 1e-5],
#                  'C': [0.001, 0.10, 0.1, 10, 25, 50, 100, 1000]},
#                {'kernel': ['sigmoid'], 'gamma': [1e-2, 1e-3, 1e-4, 1e-5],
#                  'C': [0.001, 0.10, 0.1, 10, 25, 50, 100, 1000]},
#                 {'kernel': ['linear'], 'C': [0.001, 0.10, 0.1, 10, 25, 50, 100, 1000]}
#               ]

# clf = GridSearchCV(SVC(C=1), tuned_parameters, cv=5)

classifier.fit(X_train, y_train.values.ravel())

pickle.dump(classifier, open("../models/log_reg/log_reg.pkl", 'wb'))
pickle.dump(cv, open("../models/log_reg/count_vectorizer_log_reg.pkl", 'wb'))

### Predict Class
y_pred = classifier.predict(X_test)
print(y_pred)

### Accuracy
accuracy = accuracy_score(y_test, y_pred)
print("Logistic Regression: ", accuracy)


### Classification Report
target_names = ['MajorClaim', 'Claim', 'Premise', 'None']
print(classification_report(y_test, y_pred, target_names=target_names))

# X_unseen = create_data_set(995)

# pred = classifier.predict(X_unseen)
# print(pred)
