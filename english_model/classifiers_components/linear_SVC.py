# import pandas as pd
import spacy
from sklearn.svm import LinearSVC
from sklearn.metrics import accuracy_score, classification_report
import dataframe
import pickle

nlp = spacy.load('en_core_web_sm')


cv, X_train, X_test, y_train, y_test = dataframe.create_train_test_split()


classifier = LinearSVC()

# tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-2, 1e-3, 1e-4, 1e-5],
#                  'C': [0.001, 0.10, 0.1, 10, 25, 50, 100, 1000]},
#                {'kernel': ['sigmoid'], 'gamma': [1e-2, 1e-3, 1e-4, 1e-5],
#                  'C': [0.001, 0.10, 0.1, 10, 25, 50, 100, 1000]},
#                 {'kernel': ['linear'], 'C': [0.001, 0.10, 0.1, 10, 25, 50, 100, 1000]}
#               ]

# clf = GridSearchCV(SVC(C=1), tuned_parameters, cv=5)

classifier.fit(X_train, y_train.values.ravel())

pickle.dump(classifier, open("../models/linear_SVC/linear_SVC.pkl", 'wb'))
pickle.dump(cv, open("../models/linear_SVC/count_vectorizer_linear_SVC.pkl", 'wb'))

### Predict Class
y_pred = classifier.predict(X_test)
print(y_pred)

### Accuracy
accuracy = accuracy_score(y_test, y_pred)
print("SVC: ", accuracy)


### Classification Report
target_names = ['MajorClaim', 'Claim', 'Premise', 'None']
print(classification_report(y_test, y_pred, target_names=target_names))


# X_unseen = create_data_set(995)

# pred = classifier.predict(X_unseen)
# print(pred)
