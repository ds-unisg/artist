### forward, backward, thesis or rebutal indicators before/after the component

### used from the Darmstadt paper
forward_indicators = ["as a result", "as the consequence", "because", "clearly", "consequently",
                      "considering this subject", "furthermore", "hence", "leading to the consequence", "so",
                      "taking account on this fact", "that is the reason why", "the reason is that", "therefore",
                      "this means that", "this shows that", "this will result", "thus", "thus, it is clearly seen that",
                      "thus, it is seen", "thus, the example shows"]

backward_indicators = ["additionally", "as a matter of fact", "because", "besides", "due to", "finally", "first of all",
                       "firstly", "for example", "for example", "for instance", "for instance", "furthermore",
                       "has proved it", "in addition", "in addition to this", "in the first place",
                       "is due to the fact that", "it should also be noted", "moreover", "on one hand",
                       "on the one hand", "on the other hand", "one of the main reasons", "secondly", "similarly",
                       "since", "so", "the reason", "to begin with", "to offer an instance", "what is more"]

thesis_indicators = ["all in all", "all things considered", "as far as I am concerned", "based on some reasons",
                     "by analyzing both the views", "considering both the previous fact", "finally",
                     "for the reasons mentioned above", "from explanation above", "from this point of view",
                     "i agree that", "i agree with", "i agree with the statement that", "i believe", "i believe that",
                     "i do not agree with this statement", "i firmly believe that", "i highly advocate that",
                     "i highly recommend", "i strongly believe that", "i think that", "i think the view is",
                     "i totally agree", "i totally agree to this opinion", "i would have to argue that",
                     "i would reaffirm my position that", "in conclusion", "in my opinion",
                     "in my personal point of view", "in my point of view", "in my point of view", "in summary",
                     "in the light of the facts outlined above", "it can be said that", "it is clear that",
                     "it seems to me that", "my deep conviction", "my sentiments", "overall", "personally",
                     "the above explanations and example shows that", "this, however", "to conclude",
                     "to my way of thinking", "to sum up", "ultimately"]

rebuttal_indicators = ["admittedly", "although", "besides these advantages", "but", "even though", "however",
                       "otherwise"]

### because some indicators are very long, i search if some of them exist in the input sentence. The input must be string.
### The reverse would be more complicated, e.g. how its done in argumentative_discourse_markers.py


def forward_context(s_prev):
    for f in forward_indicators:
        if f in s_prev.lower():
            return True
    return False


def backward_context(s_next):
    for b in backward_indicators:
        if b in s_next.lower():
            return True
    return False


def thesis_context(s):
    for t in thesis_indicators:
        if t in s.lower():
            return True
    return False


def rebuttal_context(s):
    for r in rebuttal_indicators:
        if r in s.lower():
            return True
    return False
