import pandas as pd
# import pickle

df = pd.read_csv("english_model/classifiers_components/analytical_base_table.csv")

### normalizing the dataframe's columns so that we don't have values that exceed 1 - this actually very slightly increased max accuracy, but significantly increased accuracy across all k folds (0.61-0.65 instead of 0.58-0.64)
df["sentence_length"] = df["sentence_length"].apply(lambda x: x / df["sentence_length"].max())
df["parse_tree_depth"] = df["parse_tree_depth"].apply(lambda x: x / df["parse_tree_depth"].max())
df["number_of_subclauses"] = df["number_of_subclauses"].apply(lambda x: x / df["number_of_subclauses"].max())
df["number_preceding_components"] = df["number_preceding_components"].apply(lambda x: x / df["number_preceding_components"].max())
df["number_following_components"] = df["number_following_components"].apply(lambda x: x / df["number_following_components"].max())

### Turning True or False to 1 or 0
bool_dict = {True: 1, False: 0}
df = df.replace(bool_dict)

y = df.loc[:, ["label"]]
X = df.drop(["label"],  axis=1)

### subtracting the mean from every column's element so that they are grouped around 0
for col in X.columns:
    X[col] = X[col].apply(lambda x: x - X[col].mean())

new_df = pd.concat([y, X], axis=1)

with pd.option_context("display.max_rows", None, "display.max_columns", None):
    print(new_df)

print(X.shape, y.shape)
print(new_df.shape)

new_df.to_csv("normalized_analytical_base_table.csv", index=None)
# new_df.to_pickle("../evaluation_tools/normalized_analytical_base_table.pkl")
