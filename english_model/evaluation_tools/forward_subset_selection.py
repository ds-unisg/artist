import pickle
import pandas as pd
from sklearn.linear_model import LogisticRegression
# from sklearn.svm import LinearSVC
from english_model.evaluation_tools import cross_validation


def print_scores():
    loaded_scores = pickle.load(open("english_model/evaluation_tools/saved_fss_scores_SVC.pkl", 'rb'))

    for i, (key, value) in enumerate(loaded_scores.items()):
        print("-" * 28)
        print("-" * 6 + F"{i} Feature Model" + "-" * 6)
        print("-" * 28)
        print(F"Model: {key}")
        print(F"Score: {value}")

        if i >= 1:
            print(F"Feature added: {key.replace(temp_key, '') if i == 1 else key.replace(temp_key, '')[2:]}")
            print(F"Difference to previous model in score: {value - temp_val}")

        temp_key = key
        temp_val = value


def forward_subset_selection(clf):
    df = pd.read_csv("english_model/classifiers_components/analytical_base_table.csv")

    y = df["label"]
    df = df.drop(["label"], axis=1)
    X_features = df.iloc[:, range(0, 43)]
    X_words = df.iloc[:, 43:]

    features = X_features.columns.tolist()
    model = []
    scores = {}

    for i in range(len(features) + 1):
        temp_scores = {}

        if i == 0:
            X_train = X_words
            scores["only words"] = cross_validation.k_fold_cross_validation(clf, X_train, y, [10]) # SVC
            print("Iteration {}: words -> {}".format(i, scores["only words"]))

        elif i == 1:
            for feature in features:
                X_train = pd.concat([X_features[feature], X_words], axis=1)
                temp_scores[feature] = cross_validation.k_fold_cross_validation(clf, X_train, y, [10]) # SVC

            features.remove(max(temp_scores.keys()))
            model.append(max(temp_scores.keys()))
            scores[', '.join(model_f for model_f in model)] = max(temp_scores.values())
            print("Iteration {}: {} -> {}".format(i, model, max(temp_scores.values())))

        else:
            for feature in features:
                X_train = X_features[feature]

                for m in model:
                    X_train = pd.concat([X_train, X_features[m]], axis=1)

                X_train = pd.concat([X_train, X_words], axis=1)
                temp_scores[feature] = cross_validation.k_fold_cross_validation(clf, X_train, y, [10]) # SVC

            features.remove(max(temp_scores.keys()))
            model.append(max(temp_scores.keys()))
            scores[', '.join(model_f for model_f in model)] = max(temp_scores.values())
            print("Iteration {}: {} -> {}".format(i, model, max(temp_scores.values())))

    with open('saved_fss_scores_SVC.pkl', 'wb') as f:
        pickle.dump(scores, f)


# SVC = LinearSVC()
log_reg = LogisticRegression()

# forward_subset_selection(log_reg)

print_scores()
