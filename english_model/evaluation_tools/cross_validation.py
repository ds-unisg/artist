import pandas as pd
from sklearn.model_selection import cross_val_score
from sklearn.svm import LinearSVC
# import pickle


"""
The following function works by passing it a classifier, the train set, labels and finally the k-folds.
The argument for k-folds is always an array, even for one number. But it can be used as such to try different
fold numbers at once.
"""


def k_fold_cross_validation(classifier, observations, labels, k_folds):
    cv_eval = []

    if len(k_folds) == 1:
        scores = cross_val_score(classifier, observations, labels, cv=k_folds[0])
        total = 0

        for score in scores:
            total += score

        # print(F"For k folds = {k_folds[0]}, accuracy is {total / len(scores)}")

        return total / len(scores)
    else:
        for k in k_folds:
            scores = cross_val_score(classifier, observations, labels, cv=k)
            total = 0

            for score in scores:
                total += score

            cv_eval.append(total / len(scores))

        # for i in range(0, len(k_folds)):
            # print(F"For k folds = {k_folds[i]}, accuracy is {cv_eval[i]}")
        return cv_eval


df = pd.read_csv(r"english_model/classifiers_components/analytical_base_table.csv")

y = df["label"]
X = df.drop(["label"], axis=1)

# clf = LinearSVC(C=2, dual=True)
clf = LinearSVC(max_iter=10000)
cv_score = k_fold_cross_validation(clf, X, y.values.ravel(), [2, 5, 10, 15, 20])
print(cv_score)
