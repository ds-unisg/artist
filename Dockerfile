FROM python:3.9-slim-buster

RUN apt-get update && \
   apt-get -y install gcc mono-mcs && \
   rm -rf /var/lib/apt/lists/*

# EXPOSE 80
COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt
RUN python -m spacy download en_core_web_sm
RUN python -m spacy download de_core_news_sm
RUN python -m nltk.downloader punkt

CMD ["python","./server.py"]
