from sklearn.ensemble import RandomForestClassifier
import spacy
from sklearn.metrics import accuracy_score, classification_report
import dataframe
import pickle
import pandas as pd


nlp = spacy.load('de_core_news_sm')


cv, X_train, X_test, y_train, y_test = dataframe.create_train_test_split()


classifier = RandomForestClassifier()
classifier.fit(X_train, y_train.values.ravel())


y_pred = classifier.predict(X_test)
print(y_pred)

# Accuracy
accuracy = accuracy_score(y_test, y_pred)
print("random forest: ", accuracy)


### Classification Report
target_names = ['Claim', 'Premise', 'None']
print(classification_report(y_test, y_pred, target_names=target_names))


pickle.dump(classifier, open("../models/random_forest/random_forest.pkl", 'wb'))
pickle.dump(cv, open("../models/random_forest/count_vectorizer_random_forest.pkl", 'wb'))





