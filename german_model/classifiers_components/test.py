from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer, TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from classifiers_components import dataframe
import pickle
from sklearn.externals import joblib
import pandas as pd
import spacy
from syntactic_features import pos, ner, dependency
import pandas as pd
import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from structural_features import sentence_closes_with_question_mark
from lexical_features import contains_adverb, contains_modal_verb
from indicators import reference_to_first_person, argumentative_discourse_markers
from dataset_analysis import analyse_dataset
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer, make_column_transformer
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.metrics import accuracy_score
from classifiers_components import dataframe
import numpy as np
import pandas as pd
import pickle
from sklearn.externals import joblib
from sklearn.preprocessing import FunctionTransformer, StandardScaler


nlp = spacy.load('de_core_news_sm')


def get_label(sentence, annotations):
  for ann in annotations:
    if ann[0] in sentence:
      assigned_label = ann[1]
      return assigned_label
  return "None"


def create_data_set(three_classes):
  f = []
  for i in range(0, 990):
    f_in = open("../Corpus/%s.txt" % i, "r")
    text = f_in.read()

    f_ann = open("../Corpus/%s.ann" % i, "r")
    line = f_ann.readline()
    annotations = []
    while line:
      l = line.split("\t")
      if l[0].startswith("T"):
        annotation = l[2]
        label = l[1].split(" ")[0]

        if not three_classes:
          if label in ('Claim', 'Premise'):
            label = 'Argumentative'
        annotations.append((annotation.strip(), label))
      line = f_ann.readline()

    # for a in annotations:
    # print(a)

    doc = nlp(text)

    sentences = [sent.string.strip() for sent in doc.sents]

    for s in sentences:
      features = {}
      sen = nlp(s)
      features["label"] = get_label(s, annotations)
      features["text"] = s
      # features["pos"] = pos.get_pos_tags(sen)
      # features["dep"] = dependency.get_dep_tags(sen)
      # features["ner"] = ner.get_ner(sen)
      features["closing_question_mark"] = sentence_closes_with_question_mark.sentence_closes_with_question_marks(sen)  # bool
      features["contains_adverb"] = contains_adverb.contains_adverb(sen)  # bool
      features["contains_modal"] = contains_modal_verb.contains_modal_verb(sen)  # bool
      features["first_person"] = reference_to_first_person.contains_first_person(sen)  # bool
      # features["causal_markers"] = argumentative_discourse_markers.contains_causal_markers(sen) # bool
      # features["conditional_markers"] = argumentative_discourse_markers.contains_conditional_markers(sen) # bool
      # features["adversative_markers"] = argumentative_discourse_markers.contains_adversative_markers(sen) # bool
      # features["consecutive_markers"] = argumentative_discourse_markers.contains_consecutive_markers(sen) # bool
      # features["concessive_markers"] = argumentative_discourse_markers.contains_concessive_markers(sen) # bool
      features["argumentative_discourse_markers"] = argumentative_discourse_markers.contains_argumentative_markers(sen)
      f.append(features)
      # print(features)

  # print(f)

  df = pd.DataFrame.from_dict(f)
  #dataset = create_dataframe(f)

  return df


text_features = ['text']
text_transformer = Pipeline(steps=[('count_vect', CountVectorizer())])

preprocessor = ColumnTransformer(transformers=[('txt', text_transformer, text_features)], remainder='passthrough')

clf = Pipeline(steps=[('preprocessor', preprocessor), ('classifier', GaussianNB())])

pipe = Pipeline([
  ('vectorize', CountVectorizer()),
  ('tfidf', TfidfVectorizer()),
  ('classify', LinearSVC())
  ])


df = create_data_set(True)

y = df.loc[:, ['label']] # y = dataset.label
X = df.drop(['label'], axis=1)

print(df.columns.values)
print(X.shape)
print(y.shape)

#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)


X_train, X_test, y_train, y_test = train_test_split(X, y.values.ravel())
clf.fit(X_train, y_train)

y_pred = clf.predict(X_test)


