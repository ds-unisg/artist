def get_tense(sentence):
    pos_list = [token.tag_ for token in sentence]

    for p in pos_list:
        if p in ('VBN', 'VBD'):
            return -1
        elif p in ('VBP', 'VBZ', 'VB'):
            return 1

    return 0