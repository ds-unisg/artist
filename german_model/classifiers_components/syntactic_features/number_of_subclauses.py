# import spacy


def num_of_subclauses(doc):
    chunks = []
    seen = set()

    for sent in doc.sents:
        heads = [cc for cc in sent.root.children if cc.dep_ == "conj"]

        for head in heads:
            words = [ww for ww in head.subtree]
            for word in words:
                seen.add(word)
            chunk = ' '.join([str(ww) for ww in words])
            chunks.append((head.i, chunk))

        unseen = [ww for ww in sent if ww not in seen]
        chunk = ' '.join([str(ww) for ww in unseen])
        chunks.append((sent.root.i, chunk))

    # chunks = sorted(chunks, key=lambda x: x[0])
    return len(chunks)
