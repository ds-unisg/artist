def contains_verb(sentence):
    verb_tag_list = ["VB", "VBD", "VBG", "VBG", "VBN", "VBP", "VBZ"]
    pos_list = [token.tag_ for token in sentence]

    for p in pos_list:
        if p in verb_tag_list:
            return True

    return False
