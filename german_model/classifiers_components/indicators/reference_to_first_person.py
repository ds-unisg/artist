def contains_first_person(sentence):
    token_list = [token.text.lower() for token in sentence]

    for t in token_list:
        if t in ("ich", "mich", "mir", "mein", "meine", "meiner", "meinen", "meinem", "meines"):
            return True

    return False
