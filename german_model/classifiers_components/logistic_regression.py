import spacy
from sklearn.svm import LinearSVC
from sklearn.metrics import accuracy_score, classification_report
import dataframe
import pickle
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
import pandas as pd
import numpy as np
from sklearn.model_selection import GridSearchCV


nlp = spacy.load('de_core_news_sm')


cv, X_train, X_test, y_train, y_test = dataframe.create_train_test_split()


# Create regularization penalty space
penalty = ['l1', 'l2']

# Create regularization hyperparameter space
C = np.logspace(0, 4, 10)

# Create hyperparameter options
hyperparameters = dict(C=C, penalty=penalty)


classifier = LogisticRegression(max_iter=10000)


#clf = GridSearchCV(classifier, hyperparameters, cv=5, verbose=0)
#best_model = clf.fit(X_train, y_train.values.ravel())

classifier.fit(X_train, y_train.values.ravel())


# Predict Class
y_pred = classifier.predict(X_test)
print(y_pred)

# Accuracy
accuracy = accuracy_score(y_test, y_pred)
print("logistic regression: ", accuracy)


### Classification Report
target_names = ['Claim', 'Premise', 'None']
print(classification_report(y_test, y_pred, target_names=target_names))


pickle.dump(classifier, open("../models/log_reg/log_reg.pkl", 'wb'))
pickle.dump(cv, open("../models/log_reg/count_vectorizer_log_reg.pkl", 'wb'))




# X_unseen = create_data_set(995)

# pred = classifier.predict(X_unseen)
# print(pred)



