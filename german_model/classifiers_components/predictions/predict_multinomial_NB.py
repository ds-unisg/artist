import spacy
import pickle
import pandas as pd
from spacy.matcher import Matcher

import sys
  
# appending a path
sys.path.append("german_model/classifiers_components")

from contextual_features import shared_phrases, type_indicators
from indicators import argumentative_discourse_markers, reference_to_first_person
from structural_features import sentence_closes_with_question_mark, sentence_length
from syntactic_features import ner, parse_tree_depth, number_of_subclauses, contains_adverb, \
    contains_verb, contains_modal_verb, tense_of_main_verb, dependency

nlp = spacy.load('de_core_news_sm')


def get_label(sentence, annotations):
    for ann in annotations:
        if ann[0] in sentence:
            assigned_label = ann[1]
            return assigned_label
    return "None"


"""The following 2 functions are relevant for the contextual features"""


### This function returns all the noun chunks of a sentence - used for the introduction and conclusion
def noun_chunks(sen):
    nouns = []
    for noun in sen.noun_chunks:
        nouns.append(noun)

    return nouns


### This function returns the verb phrases of a sentence - used for the introduction and conclusion
def verb_phrases(sen):
    ### found those patterns at https://stackoverflow.com/questions/47856247/extract-verb-phrases-using-spacy
    ### maybe we can use more/less
    pattern = [{'POS': 'VERB', 'OP': '?'},
               {'POS': 'ADV', 'OP': '*'},
               {'POS': 'AUX', 'OP': '*'},
               {'POS': 'VERB', 'OP': '+'}]

    ### instantiate a Matcher instance
    matcher = Matcher(nlp.vocab)
    matcher.add("Verb phrase", [pattern])

    ### call the matcher to find matches
    #verbPhrases = [sen[start:end] for _, start, end in matcher(sen)]

    return ["This"]#verbPhrases

### Predict Class
# y_pred = classifier.predict(X_test)
# print(y_pred)

### Accuracy
# accuracy = accuracy_score(y_test, y_pred)
# print("NB: ", accuracy)


def create_data_set(text, cv):
    f = []

    # read text
    # f_in = open("../Corpus/%s.txt" % i, "r")
    # text = f_in.read()

    doc = nlp(text)

    sentences = [sent for sent in doc.sents]

    ### calculating the average tokens of a sentence - relative for Structural Features
    total = 0

    for p in range(len(sentences)):
        total += len(sentences[p])

    avg_sent_length = total / len(sentences)

    ### to be used in some contextual features (from shared_phrases) - can also be moved away from the dataframe
    ### into the features but that would mean sending unnecessarily sending a lot of sentences
    ### relative for Contextual Features
    intro_nouns = noun_chunks(sentences[0])
    conc_nouns = noun_chunks(sentences[len(sentences) - 1])
    intro_verbs = verb_phrases(sentences[0])
    conc_verbs = verb_phrases(sentences[len(sentences) - 1])

    ### extracting the number of subclauses from the whole document - relative for Syntactic Features
    total_subclauses = number_of_subclauses.num_of_subclauses(doc)

    ### max dependency tree depth for the whole document - relative for Syntactic Features
    doc_parse_tree_depth = parse_tree_depth.parse_tree_depth(doc)

    for j in range(len(sentences)):
        features = {}

        if sentences[j] is not None:

            sen = str(sentences[j])

            features["text"] = sen

            """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ STRUCTURAL FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

            ### newly added - greatly (!) increased accuracy
            if j == 0:
                features["is_first"] = True
            else:
                features["is_first"] = False

            if j == len(sentences) - 1:
                features["is_last"] = True
            else:
                features["is_last"] = False

            features["relative_position"] = j / len(sentences)
            features["number_preceding_components"] = j
            features["number_following_components"] = len(sentences) - j - 1
            features["token_ratio"] = len(sentences[j]) / avg_sent_length  # numeric
            features["sentence_length"] = sentence_length.get_sentence_length(sentences[j])  # numeric
            features["closing_question_mark"] = sentence_closes_with_question_mark. \
                sentence_closes_with_question_marks(sentences[j])  # bool

            """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ INDICATOR FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

            features["causal_markers"] = argumentative_discourse_markers.contains_causal_markers(
                sentences[j])  # bool
            features["conditional_markers"] = argumentative_discourse_markers.contains_conditional_markers(
                sentences[j])  # bool
            features["adversative_markers"] = argumentative_discourse_markers.contains_adversative_markers(
                sentences[j])  # bool
            features["consecutive_markers"] = argumentative_discourse_markers.contains_consecutive_markers(
                sentences[j])  # bool
            features["concessive_markers"] = argumentative_discourse_markers.contains_concessive_markers(
                sentences[j])  # bool
            features["argumentative_discourse_markers"] = \
                argumentative_discourse_markers.contains_argumentative_markers(sentences[j])  # bool
            features["first_person"] = reference_to_first_person.contains_first_person(sentences[j])  # bool

            """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ CONTEXTUAL FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

            ### newly added - looking for argumentation in the preceding component. if the component is
            ### the first sentence of the document, the values are the same as the next 4 features
            if j - 1 >= 0:
                features["preceding_thesis_indicator"] = type_indicators.thesis_context(str(sentences[j - 1]))
                features["preceding_rebuttal_inidcator"] = type_indicators.rebuttal_context(str(sentences[j - 1]))
                features["preceding_forward_context"] = type_indicators.forward_context(str(sentences[j - 1]))
                features["preceding_backward_context"] = type_indicators.backward_context(str(sentences[j - 1]))
            elif j == 0:
                features["preceding_thesis_indicator"] = False  # type_indicators.thesis_context(sen)
                features["preceding_rebuttal_inidcator"] = False  # type_indicators.rebuttal_context(sen)
                features["preceding_forward_context"] = False  # type_indicators.forward_context(sen)
                features["preceding_backward_context"] = False  # type_indicators.backward_context(sen)

            ### newly added
            features["thesis_indicator"] = type_indicators.thesis_context(sen)  # bool
            features["rebuttal_indicator"] = type_indicators.rebuttal_context(sen)  # bool
            features["forward_context"] = type_indicators.forward_context(sen)  # bool
            features["backward_context"] = type_indicators.backward_context(sen)  # bool

            ### newly added - looking for argumentation in the following component. if the component is
            ### the last sentence of the document, the values are the same as the previous 4 features
            if j + 1 <= len(sentences) - 1:
                features["following_thesis_indicator"] = type_indicators.thesis_context(str(sentences[j + 1]))
                features["following_rebuttal_inidcator"] = type_indicators.rebuttal_context(str(sentences[j + 1]))
                features["following_forward_context"] = type_indicators.forward_context(str(sentences[j + 1]))
                features["following_backward_context"] = type_indicators.backward_context(str(sentences[j + 1]))
            elif j == len(sentences) - 1:
                features["following_thesis_indicator"] = False  # type_indicators.thesis_context(sen)
                features["following_rebuttal_inidcator"] = False  # type_indicators.rebuttal_context(sen)
                features["following_forward_context"] = False  # type_indicators.forward_context(sen)
                features["following_backward_context"] = False  # type_indicators.backward_context(sen)

            ### comparing noun chunks of component with noun chunks in first and last sentences of essay, return
            ### the number of matches
            features["intro_shared_noun_chunks"] = shared_phrases.shared_noun_phrases(sentences[j],
                                                                                      intro_nouns)  # numeric
            features["conc_shared_noun_chunks"] = shared_phrases.shared_noun_phrases(sentences[j],
                                                                                     conc_nouns)  # numeric

            ### comparing verb phrases of component with verb phrases in first and last sentences of essay, return
            ### the number of matches
            features["intro_shared_verb_phrases"] = shared_phrases.shared_verb_phrases(sentences[j],
                                                                                       intro_verbs)  # numeric
            features["conc_shared_verb_phrases"] = shared_phrases.shared_verb_phrases(sentences[j],
                                                                                      conc_verbs)  # numeric

            ### the number of total nouns and verb phrases in the introduction and conclusion sentences
            features["intro_noun_chunks"] = len(intro_nouns)
            features["conc_noun_chunks"] = len(conc_nouns)
            features["intro_verb_phrases"] = len(intro_verbs)
            features["conc_verb_phrases"] = len(conc_verbs)

            """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ SYNTACTIC AND OTHER FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

            features["contains_adverb"] = contains_adverb.contains_adverb(sentences[j])  # bool
            features["contains_modal"] = contains_modal_verb.contains_modal_verb(sentences[j])  # bool
            features["contains_verb"] = contains_verb.contains_verb(sentences[j])  # bool
            features["contains_named_entities"] = ner.contains_ner(sentences[j])  # bool

            ### newly added
            features["parse_tree_depth"] = doc_parse_tree_depth  # numeric
            features["sentence_dependency_depth"] = dependency.tree_depth(sentences[j])
            features["tense"] = tense_of_main_verb.get_tense(
                sentences[j])  # numeric {"-1": past, "0": present, "1": future}
            features["number_of_subclauses"] = total_subclauses  # numeric

            # features["pos"] = pos.get_pos_tags(sen)
            # features["dep"] = dependency.get_dep_tags(sen)
            # features["ner"] = ner.get_ner(sen) # used above as a boolean feature

            f.append(features)

    df = pd.DataFrame.from_dict(f)

    X1 = cv.transform(df.text)
    df = df.drop(columns='text')
    count_vect_df = pd.DataFrame(X1.todense(), columns=cv.get_feature_names())

    combined_df = pd.concat([df, count_vect_df], axis=1)

    return combined_df


def predict(text):
    ### load the model from disk
    loaded_model = pickle.load(open("german_model/models/multinomial_NB/multinomial_NB.pkl", 'rb'))
    loaded_count_vectorizer = pickle.load(open("german_model/models/multinomial_NB/count_vectorizer_multinomial_NB.pkl", 'rb'))

    X_unseen = create_data_set(text, loaded_count_vectorizer)

    pred = loaded_model.predict(X_unseen)
    print(pred)
    return pred


#i = 402
#f_in = open("../data/Corpus/%s.txt" % i, "r")
#text = f_in.read()
#print(predict(text))
